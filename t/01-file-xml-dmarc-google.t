#! /usr/bin/env perl6

use v6.c;

use Test;
use File::XML::DMARC::Google;

plan 3;

my %report = File::XML::DMARC::Google.new("t/files/google.com!zaaksysteem.nl!1538265600!1538351999.xml").contents;

subtest "metadata", {
	my @tests = < org-name email extra-contact-info report-id >;

	plan 2 + @tests.elems;

	my %expected = %(
		date-range => %(begin => DateTime.new(1538265600), end => DateTime.new(1538351999)),
		email => 'noreply-dmarc-support@google.com',
		extra-contact-info => "https://support.google.com/a/answer/2466580",
		org-name => "google.com",
		report-id => "16138384843935024300",
	);

	for @tests {
		is %report<metadata>{$_}, %expected{$_}, "metadata.$_ is correct";
	}

	is %report<metadata><date-range><begin>, %expected<date-range><begin>, "metadata.date-range.begin is correct";
	is %report<metadata><date-range><end>, %expected<date-range><end>, "metadata.date-range.end is correct";
}

subtest "policy", {
	my %expected = %(
		dkim => "r",
		domain => "zaaksysteem.nl",
		p => "none",
		pct => "100",
		sp => "none",
		spf => "r",
	);

	plan %expected.keys.elems;

	for %expected.keys {
		is %report<policy>{$_}, %expected{$_}, "policy.$_ is correct";
	}
}

subtest "records", {
	my %expected = %(
		identifiers => %(
			from => "zaaksysteem.nl",
		),
		row => %(
			source-ip => "185.54.113.15",
			count => 6,
			evaluated => %(
				disposition => "none",
				dkim => False,
				spf => True,
			),
		),
		results => %(
			spf => %(
				domain => "zaaksysteem.nl",
				result => "pass",
			),
		),
	);

	my $record;

	for %report<records>.list {
		next unless $_<identifiers><from> eq "zaaksysteem.nl";

		$record = $_;
		last;
	}

	plan 2;

	ok $record, "zaaksysteem.nl record found";
	is-deeply $record, %expected, "Record contents are correct";
}

# vim: ft=perl6 noet
