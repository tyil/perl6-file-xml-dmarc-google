#! /usr/bin/env false

use v6.c;

use XML::XPath;

unit class File::XML::DMARC::Google;

has IO::Path $.path;
has %!cache;

#| Retrieve the DMARC report as a Hash.
method contents (
	#| Toggle whether to use caching for this method.
	Bool:D :$cache = True,

	--> Hash
) {
	return %!cache<contents> if $cache && %!cache<contents>;

	my %report = %(
		metadata => %(
			org-name => self.xpath("/feedback/report_metadata/org_name"),
			email => self.xpath("/feedback/report_metadata/email"),
			extra-contact-info => self.xpath("/feedback/report_metadata/extra_contact_info"),
			report-id => self.xpath("/feedback/report_metadata/report_id"),
			date-range => %(
				begin => DateTime.new(+self.xpath("/feedback/report_metadata/date_range/begin")),
				end => DateTime.new(+self.xpath("/feedback/report_metadata/date_range/end")),
			)
		),
		policy => %(
			domain => self.xpath("/feedback/policy_published/domain"),
			dkim => self.xpath("/feedback/policy_published/adkim"),
			spf => self.xpath("/feedback/policy_published/aspf"),
			p => self.xpath("/feedback/policy_published/p"),
			sp => self.xpath("/feedback/policy_published/sp"),
			pct => self.xpath("/feedback/policy_published/pct"),
		),
		records => [],
	);

	for 1..self.record-count {
		%report<records>.push: self.record($_);
	}

	%!cache<contents> = %report if $cache;

	%report;
}

#| Retrieve the contents as XML::XPath object. It is recommended to use the
#| contents method instead.
method contents-xml (
	#| Toggle whether to use caching for this method.
	Bool:D :$cache = True,

	--> XML::XPath
) {
	return %!cache<xml> if $cache && %!cache<xml>;

	my $doc = XML::XPath.new(file => $!path.absolute);

	%!cache<xml> = $doc if $cache;

	$doc;
}

#| Retrieve a single record of the DMARC report. You must supply an index to
#| the record you want to retrieve.
method record (
	Int:D $index,
	--> Hash
) {
	my $xpath = "/feedback/record[$index]";
	my %record = %(
		row => %(
			source-ip => self.xpath("$xpath/row/source_ip"),
			count => (self.xpath("$xpath/row/count") // 0).Int,
			evaluated => %(
				disposition => self.xpath("$xpath/row/policy_evaluated/disposition"),
				dkim => self.xpath("$xpath/row/policy_evaluated/dkim") eq "pass",
				spf => self.xpath("$xpath/row/policy_evaluated/spf") eq "pass",
			),
		),
		identifiers => %(
			from => self.xpath("$xpath/identifiers/header_from"),
		),
		results => %(),
	);

	try {
		%record<results><dkim> = %(
			domain => self.xpath("$xpath/auth_results/dkim/domain"),
			result => self.xpath("$xpath/auth_results/dkim/result") eq "pass",
			selector => self.xpath("$xpath/auth_results/dkim/selector"),
		);
	}

	try {
		%record<results><spf> = %(
			domain => self.xpath("$xpath/auth_results/spf/domain"),
			result => self.xpath("$xpath/auth_results/spf/result"),
		);
	}

	%record;
}

#| Get the number of records in the report.
method record-count (
	--> Int
) {
	self.contents-xml.find("/feedback/record").elems;
}

#| Retrieve a value from the XML document via it's XPath.
method xpath (
	#| The XPath to access the value.
	Str:D $xpath,

	#| Toggle whether to use caching for this method.
	Bool:D :$cache = True,

	--> Str
) {
	return %!cache<xpath>{$xpath} if $cache && %!cache<xpath>{$xpath};

	my $lookup = self.contents-xml(:$cache).find($xpath);
	my $value = $lookup
		?? $lookup[0].string
		!! Failure.new("No element found with XPath $xpath")
		;

	%!cache<xpath>{$xpath} if $cache;

	$value;
}

#| Create a new object that wraps around an XML based DMARC report from Google.
multi method new (
	IO::Path:D $path,
) {
	self.bless(:$path);
}

#| Create a new object that wraps around an XML based DMARC report from Google.
multi method new (
	Str:D $path,
) {
	samewith($path.IO);
}

=begin pod

=NAME    File::XML::DMARC::Google
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.1.1

=head1 Description

A class to wrap around a DMARC report from Google. These are generally XML
files, contained in a zip file. To handle the zip file, you can take a look at
the C<File::Zip> module.

=head1 Examples

=begin code
use File::XML::DMARC::Google;

my File::XML::DMARC::Google $xml .= new: "/tmp/google-dmarc.xml";
my %report = $xml.contents;
=end code

The C<%report> variable contains a Perl 6 hash from which you can easily access
all the information you need from the DMARC report.

=head1 See also

=item1 L<C<File::Zip>|https://gitlab.com/tyil/perl6-file-zip>

=end pod

# vim: ft=perl6 noet
