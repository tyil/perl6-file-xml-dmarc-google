=begin pod

=NAME    File::XML::DMARC::Google
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.1.1

=head1 Description

Parser for XML-formatted DMARC reports from Google

=head1 Installation

Install this module through L<zef|https://github.com/ugexe/zef>:

=begin code :lang<sh>
zef install File::XML::DMARC::Google
=end code

=head1 License

This module is distributed under the terms of the AGPL-3.0.

=end pod
